Require Import ZArith.
Require Import Coq.Lists.List.
Import ListNotations.
Local Open Scope Z_scope.

(* Summation over lists of integers. *)

Section Specification.

  Inductive is_sum : list Z -> Z -> Prop :=
  | is_sum_nil : is_sum [] 0
  | is_sum_cons : forall y n ns, is_sum ns y -> is_sum (n::ns) (n + y).

  (* Convince ourselves that the spec is right. *)

  Example is_sum_ex1 :
    is_sum [1;2;3] 6.
  Proof.
    replace 6 with (1 + (2 + (3 + 0))) by reflexivity.
    repeat constructor.
  Qed.

  Theorem is_sum_append :
    forall ns n_sum ms m_sum,
      is_sum ns n_sum ->
      is_sum ms m_sum ->
      is_sum (ns ++ ms) (n_sum + m_sum).
  Proof.
    intros ns n_sum ms m_sum Hns Hms.
    generalize dependent n_sum.
    induction ns;
      destruct ms;
      intros;
      inversion Hns;
      inversion Hms.
    - constructor.
    - simpl.
      constructor.
      auto.
    - rewrite <- app_nil_end.
      rewrite Z.add_0_r.
      constructor.
      auto.
    - rewrite <- app_comm_cons.
      rewrite <- Z.add_assoc.
      constructor.
      replace m_sum in IHns.
      auto.
  Qed.

  Theorem sum_func :
    forall ns, exists! y, is_sum ns y.
  Proof.
    intros ns.
    induction ns.
    - exists 0; split.
      + constructor.
      + intros x' H.
        inversion H.
        reflexivity.
    - inversion IHns as (x,(Hx,Ux)).
      exists (a + x).
      split.
      + constructor.
        assumption.
      + intros x' Hx'.
        inversion Hx'.
        subst.
        rewrite Ux with y by assumption.
        reflexivity.
  Qed.

End Specification.


Section Program.

  (* A program to *compute* the sum of a list of integers, using
     constant space and time linear in the size of the list. *)

  Fixpoint sum_loop acc ns :=
    match ns with
    | [] => acc
    | n::ns' => sum_loop (n + acc) ns'
    end.

  Definition sum := sum_loop 0.

  Compute (sum [1;2;3]).

End Program.


(* Proof that the program meets the spec. *)

Lemma sum_loop_is_sum :
  forall ns acc y,
    sum_loop acc ns = y + acc -> is_sum ns y.
Proof.
  intro ns.
  induction ns;
    simpl;
    intros acc y H.
  - replace y with 0 by intuition.
    constructor.
  - replace y with (a + (y - a)) by intuition.
    constructor.
    apply IHns with (a + acc).
    replace (y - a + (a + acc)) with (y + acc) by intuition.
    assumption.
Qed.

Theorem sum_is_sum :
  forall ns y,
    sum ns = y -> is_sum ns y.
Proof.
  intros ns y H.
  apply sum_loop_is_sum with 0.
  replace (y + 0) with y by intuition.
  assumption.
Qed.

(* Our "proof certificates" *)
Print sum_is_sum.
Print sum_loop_is_sum.
