#include <stdlib.h>

/*
 * To check, run:
 * $ frama-c -rte -wp sum.c
*/

/*@
  @predicate Unchanged{K,L}(int *a, integer first, integer last) =
  @  \forall integer i; first <= i < last ==> \at(a[i],K) == \at(a[i],L);
  @
  @predicate Unchanged{K,L}(int *a, integer n) =
  @  Unchanged{K,L}(a, 0, n);
  @
  @axiomatic AccumulateAxiomatic {
  @logic int Accumulate{L}(int *a, integer n, int init) reads a[0..n-1];
  @
  @axiom AccumulateEmpty:
  @  \forall int *a, init, integer n;
  @  n <= 0 ==> Accumulate(a, n, init) == init;
  @
  @axiom AccumulateNext:
  @  \forall int *a, init, integer n;
  @  0 <= n ==> Accumulate(a, n+1, init) == Accumulate(a, n, init) + a[n];
  @}
  @
  @predicate AccumulateBounds{L}(int *a, integer n, int init) =
  @  \forall integer i; 0 <= i <= n ==> INT_MIN <= Accumulate(a, i, init) <= INT_MAX;
  @
  @predicate AccumulateBounds{L}(int *a, integer n) =
  @  AccumulateBounds{L}(a, n, (int)0);
  @*/


/*
  NB Originally this file had

  axiom AccumulateEmpty:
  \forall int *a, init, integer n;
  0 <= n ==> Accumulate(a, n, init) == init;

  which caused

  ensures result: \result == Accumulate(a, n+1, init);

  to pass. This was very confusing and shows the importance of getting
  the spec right!
*/


/*@requires valid: \valid_read(a + (0..n-1));
  @requires bounds: AccumulateBounds(a, n, init);
  @assigns \nothing;
  @ensures result: \result == Accumulate(a, n, init);
  @*/
int accumulate(const int *a, unsigned int n, int init) {
  /*@loop invariant index: 0 <= i <= n;
    @loop invariant partial: init == Accumulate(a, i, \at(init,Pre));
    @loop assigns i, init;
    @loop variant n-i;
    @*/
  for(unsigned int i = 0; i < n; ++i) {
    init = init + a[i];
  }
  return init;
}

/*@requires valid: \valid_read(a + (0..n-1));
  @requires bounds: AccumulateBounds(a, n);
  @assigns \nothing;
  @ensures result: \result == Accumulate(a, n, (int)0);
  @*/
int sum(const int *a, unsigned int n) {
  return accumulate(a, n, 0);
}
